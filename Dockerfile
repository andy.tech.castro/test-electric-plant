FROM golang as base

WORKDIR /app

ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY main.go .

# it will take the flags from the environment
RUN go build -o app
RUN ls

### App
FROM scratch as app
COPY --from=base app /
EXPOSE 8888
ENTRYPOINT ["/app"]
