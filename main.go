package main

import (
	"encoding/json"
	"log"
	"math"
	"net/http"
	"os"
	"sort"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
)

//Struct for json Input (Payload)
type Payload struct {
	Load        float64       `json:"load"`
	Fuels       Fuels         `json:"fuels"`
	PowerPlants []PowerPlants `json:"powerplants"`
}

//Struct for fuels
type Fuels struct {
	Gas      float64 `json:"gas(euro/MWh)"`
	Kerosine float64 `json:"kerosine(euro/MWh)"`
	Co2      float64 `json:"co2(euro/ton)"`
	Wind     float64 `json:"wind(%)"`
}

//Struct for power plants
type PowerPlants struct {
	Name       string  `json:"name"`
	Type       string  `json:"type"`
	Efficiency float64 `json:"efficiency"`
	Pmin       int16   `json:"pmin"`
	Pmax       int16   `json:"pmax"`
	value      float64
}

//Struct for output
type Output struct {
	Name string  `json:"name"`
	P    float64 `json:"p"`
}

var broadcastIn = make(chan *Payload)   //Channel for send Payload to websocket
var broadcastOut = make(chan *[]Output) //Channel for send out to websocket
var clients = make(map[*websocket.Conn]bool)

var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func main() {
	file, errLog := os.OpenFile("logs.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if errLog != nil {
		log.Fatal(errLog)
	}
	log.SetOutput(file)
	router := mux.NewRouter()
	router.HandleFunc("/productionplan", getData).Methods("POST") //Endpoint for receive data
	router.HandleFunc("/websocket", responseWS)                   //Websocket endpoint
	go echo()
	log.Fatal(http.ListenAndServe(":8888", router))
}

func getData(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var payload Payload
	err := json.NewDecoder(r.Body).Decode(&payload)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	// Send Payload to websocket
	go writerPayload(&payload)

	outputJSON, _ := json.MarshalIndent(payload.ChoosePlants(), "", "\t")
	w.Write(outputJSON)
}

func (py Payload) ChoosePlants() []Output {
	//I use merit order, environment dispatch at first, thats why i start with windtubine type
	var sources []PowerPlants
	var fuelSources []PowerPlants
	for _, powerPlant := range py.PowerPlants {
		//Choose firs windturbine type in slice separate from others because i choose the order in different way
		if powerPlant.Type == "windturbine" {
			powerPlant.value = py.Fuels.Wind * 1 / powerPlant.Efficiency
			sources = append(sources, powerPlant)
		} else if powerPlant.Type == "gasfired" {
			powerPlant.value = py.Fuels.Gas * 1 / powerPlant.Efficiency
			fuelSources = append(fuelSources, powerPlant)
		} else if powerPlant.Type == "turbojet" {
			powerPlant.value = py.Fuels.Kerosine * 1 / powerPlant.Efficiency
			fuelSources = append(fuelSources, powerPlant)
		}
	}
	// In windturbine type i put firt the most efficient turbine
	sort.SliceStable(sources, func(i, j int) bool {
		return sources[i].value > sources[j].value
	})
	// In the other i put first cheaper
	sort.SliceStable(fuelSources, func(i, j int) bool {
		return fuelSources[i].value < fuelSources[j].value
	})
	//I mix the slices for do only one for
	sources = append(sources, fuelSources...)
	var totalEnergy float64
	var outputs []Output //Create an slice for create a group for output structs
	var energyProduction float64
	var counter int8
	for _, powerPlant := range sources {
		if powerPlant.Type == "windturbine" {
			//i choose de energy production dependient of the wind
			energyProduction = (powerPlant.value / 100) * float64(powerPlant.Pmax)
		} else {
			// I choose the max energy production if i need
			energyProduction = float64(powerPlant.Pmax)
		}
		var output Output
		output.Name = powerPlant.Name
		//If Load is complete the energy for this source will be 0
		if totalEnergy == py.Load {
			output.P = 0
		} else {
			totalEnergy += energyProduction
			if totalEnergy < py.Load { //If i can produce all the energy with this plant
				output.P = math.Round(energyProduction*100) / 100
			} else if totalEnergy > py.Load { //If i need produce less than the max
				totalEnergy -= energyProduction
				restEnergy := py.Load - totalEnergy
				if restEnergy >= float64(powerPlant.Pmin) {
					output.P = math.Round(restEnergy*100) / 100
				} else {
					//If the minimun energy is too many for the rest of the energy i chage the last
					restPreview := float64(powerPlant.Pmin) - restEnergy
					outputs[counter-1].P -= restPreview
					output.P = math.Round(float64(powerPlant.Pmin)*100) / 100
				}
				totalEnergy += restEnergy
			}
		}
		outputs = append(outputs, output)
		counter++
	}

	//Send Output to Websocket
	go writerOutput(&outputs)
	return outputs
}
func responseWS(w http.ResponseWriter, r *http.Request) {
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println("Upgrade:", err)
		return
	}

	//Adding websocket client info
	clients[ws] = true
}

func writerPayload(output *Payload) {
	broadcastIn <- output
}

func writerOutput(output *[]Output) {
	broadcastOut <- output
}

//Send Payload and Output to websockets
func echo() {
	for {
		val := <-broadcastIn
		val2 := <-broadcastOut
		for client := range clients {
			//Sending info to each websocket client
			errIn := client.WriteJSON(val) //Payload
			client.WriteJSON(val2)         //Output
			if errIn != nil {
				log.Printf("Websocket error: %s", errIn)
				client.Close()
				delete(clients, client)
			}
		}
	}
}
