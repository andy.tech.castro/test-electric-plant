# Test Powerplant

## Language
The application is programmed in Go so you must have go installed

## Build
You must install a dependency with the next code
```
go get -u github.com/gorilla/mux
```

After that you can build it with
```
go build main.go
```

For launch the app it depends of your OS
### Linux
```
./main
```
### Windows
```
./main.exe
```

## Docker
If you prefer docker you can build it too with the next code

```
docker build -t test -f Dockerfile .
```
And run it with
```
docker run -it --rm -p 8888:8888 test
```

## Websocket
There is a websocket in /websocket

